// Array16.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int x = 12;
    int array1[x][x];

    int width;
    int height;

    std::cout << "\n Hello! Today is a great day!\n\n" <<
        " This is " << x << "x" << x << " array: \n\n";

    for (height = 0; height < x; height++)
    {
        for (width = 0; width < x; width++)
        {
            array1[width][height] = (height - 1) + (width + 1);

            if(array1[width][height] < 10)
                std::cout << ' ' << array1[width][height] << ' ';
            else
                std::cout << array1[width][height] << ' ';
        }
        std::cout << '\n';
    }
        
    int sumString = 0;
    int condition = buf.tm_mday % x;

    for (width = 0; width < x; width++)
    {
        sumString = sumString + array1[width][condition];
    }
    std::cout << "\n Today is day " << buf.tm_mday <<
        " and " << buf.tm_mday << "%" << x << "=" << condition << 
        "\n so, let's get sum of string index " << condition <<
        "\n Result: "<< sumString << std::endl;
    
    return 0;
}
